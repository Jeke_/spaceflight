// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DMGameMode.generated.h"

/**
 * 
 */
UCLASS()
class SPACEFLIGHT_API ADMGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	/*ADMGameMode();*/
	
	int32 PlayerCount = 0;

	//float GameLength = 1800; // 30min
	float GameLength = 5; // 30min

	TMap<AController*, int32> PlayerControllers;

	TMap<AController*, int32> ControllerStrikes;

	ADMGameMode();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	virtual void RestartPlayer(AController* NewPlayer) override;
	
	virtual void RestartPlayerAtPlayerStart(AController* NewPlayer, AActor* StartSpot) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;

	UFUNCTION()
	void TimeDiscrepancyStrike(AController* Controller);
};
