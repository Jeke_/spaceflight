// Fill out your copyright notice in the Description page of Project Settings.

#include "Actors/ShipPawn.h"
#include "GameFramework/GameNetworkManager.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Engine/World.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AShipPawn::AShipPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AShipPawn::BeginPlay()
{
	Super::BeginPlay();
	
	if(!HasAuthority())
	{
		NetCullDistanceSquared = FLT_MAX;
	}
	else
	{
		SetReplicates(true);
	}
}

void AShipPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	
	DOREPLIFETIME(AShipPawn, Health);
}

// Called every frame
void AShipPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(!HasAuthority())
	{
		//CompileControlValues();
		HandleForwardMovement();
	}
}

// Called to bind functionality to input
void AShipPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("Pitch", this, &AShipPawn::Pitch);
	PlayerInputComponent->BindAxis("Roll", this, &AShipPawn::Roll);
	PlayerInputComponent->BindAxis("Yaw", this, &AShipPawn::Yaw);

	PlayerInputComponent->BindAction("EngineIdle", IE_Pressed, this, &AShipPawn::EngineIdle);
	PlayerInputComponent->BindAction("EngineSlow", IE_Pressed, this, &AShipPawn::EngineSlow);
	PlayerInputComponent->BindAction("EngineFast", IE_Pressed, this, &AShipPawn::EngineHigh);
	PlayerInputComponent->BindAction("EngineFull", IE_Pressed, this, &AShipPawn::EngineFull);
}

void AShipPawn::Pitch(float AxisValue)
{
	if(AxisValue > PitchDeadzone || AxisValue < -PitchDeadzone)
	{
		CurrentPitch = AxisValue * PitchRate * GetWorld()->GetDeltaSeconds() * -1;
		AddControllerPitchInput(CurrentPitch);
	}
	else
	{
		CurrentPitch = 0.f;
	}
}

void AShipPawn::Roll(float AxisValue)
{
	if (AxisValue > RollDeadzone || AxisValue < -RollDeadzone)
	{
		CurrentRoll = AxisValue * RollRate * GetWorld()->GetDeltaSeconds();
		AddControllerRollInput(CurrentRoll);
	}
	else
	{
		CurrentRoll = 0.f;
	}
}

void AShipPawn::Yaw(float AxisValue)
{
	if (AxisValue > YawDeadzone || AxisValue < -YawDeadzone)
	{
		CurrentYaw = AxisValue * YawRate * GetWorld()->GetDeltaSeconds();
		AddControllerYawInput(CurrentYaw);
	}
	else
	{
		CurrentYaw = 0.f;
	}
}

void AShipPawn::EngineIdle()
{
	EngineSetting = EThrottleMode::Idle;
	EngineChange.Broadcast(EngineSetting);
}

void AShipPawn::EngineSlow()
{
	EngineSetting = EThrottleMode::Slow;
	EngineChange.Broadcast(EngineSetting);
}

void AShipPawn::EngineHigh()
{
	EngineSetting = EThrottleMode::High;
	EngineChange.Broadcast(EngineSetting);
}

void AShipPawn::EngineFull()
{
	EngineSetting = EThrottleMode::Full;
	EngineChange.Broadcast(EngineSetting);
}

void AShipPawn::CompileControlValues()
{
	FRotator ControlValues(CurrentPitch, CurrentYaw, CurrentRoll);
	FQuat DeltaRotation = EulerToQuaternion(ControlValues);
	AddActorLocalRotation(DeltaRotation);

	HandleForwardMovement();
}

void AShipPawn::HandleForwardMovement()
{
	float EngineModifier = 0.f;
	switch (EngineSetting)
	{
		case EThrottleMode::Slow:
			{
				EngineModifier = 0.33f;
				break;
			}
		case EThrottleMode::High:
			{
				EngineModifier = 0.66f;
				break;
			}
		case EThrottleMode::Full:
			{
				EngineModifier = 1.f;
				break;
			}
		case EThrottleMode::Idle:
			{
				EngineModifier = 0.f;
			}
	}

	AddMovementInput(GetActorForwardVector(), EngineModifier);
}

void AShipPawn::SetInputComponent()
{
	UInputComponent* ShipInputComponent = Cast<UInputComponent>(FindComponentByClass(UInputComponent::StaticClass()));
	if(ShipInputComponent)
	{

		ShipInputComponent->BindAction("EngineIdle", IE_Pressed, this, &AShipPawn::EngineIdle);
		ShipInputComponent->BindAction("EngineSlow", IE_Pressed, this, &AShipPawn::EngineSlow);
		ShipInputComponent->BindAction("EngineFast", IE_Pressed, this, &AShipPawn::EngineHigh);
		ShipInputComponent->BindAction("EngineFull", IE_Pressed, this, &AShipPawn::EngineFull);
		
		ShipInputComponent->BindAxis("Pitch", this, &AShipPawn::Pitch);
		ShipInputComponent->BindAxis("Roll", this, &AShipPawn::Roll);
		ShipInputComponent->BindAxis("Yaw", this, &AShipPawn::Yaw);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("No inputcomponent"));
	}
}

// Formula to convert a Euler angle in degrees to a quaternion rotation
FQuat AShipPawn::EulerToQuaternion(FRotator Current_Rotation)
{
	FQuat q;                                            // Declare output quaternion
	float Yaw = Current_Rotation.Yaw * PI / 180;        // Convert degrees to radians
	float Roll = Current_Rotation.Roll * PI / 180;
	float Pitch = Current_Rotation.Pitch * PI / 180;

	double cy = cos(Yaw * 0.5);
	double sy = sin(Yaw * 0.5);
	double cr = cos(Roll * 0.5);
	double sr = sin(Roll * 0.5);
	double cp = cos(Pitch * 0.5);
	double sp = sin(Pitch * 0.5);

	q.W = cy * cr * cp + sy * sr * sp;
	q.X = cy * sr * cp - sy * cr * sp;
	q.Y = cy * cr * sp + sy * sr * cp;
	q.Z = sy * cr * cp - cy * sr * sp;

	return q;                                           // Return the quaternion of the input Euler rotation
}

