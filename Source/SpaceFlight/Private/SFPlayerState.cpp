// Fill out your copyright notice in the Description page of Project Settings.


#include "SFPlayerState.h"
#include "SpaceFlight/Public/Actors/ShipPawn.h"
#include "SpaceFlight/DMGameMode.h"

void ASFPlayerState::FireWeaponMC_Implementation()
{
	if(!ControlledPawn)
	{
		FindPawn();
		return;
	}

	if(!HasAuthority())
	{
		ControlledPawn->FireBeam.Broadcast();
	}
	
}

void ASFPlayerState::ServerFireWeapon_Implementation()
{
	if(HasAuthority())
	{
		FireWeaponMC();
	}
}

void ASFPlayerState::ServerNotifyHit_Implementation(AShipPawn* TargetHit)
{
	if(HasAuthority())
	{
		TargetHit->Health -= 10;
		if(TargetHit->Health <= 0)
		{
			ADMGameMode* GameMode = Cast<ADMGameMode>(GetWorld()->GetAuthGameMode());
			GameMode->RestartPlayer(TargetHit->Controller);
		}
	}
}

void ASFPlayerState::RPCShowHackWarning_Implementation(bool Show, float Progress)
{
	if(!HasAuthority())
	{
		if(ControlledPawn != nullptr)
		{
			ControlledPawn->WarningUpdate.Broadcast(Show, Progress);
		}
		else
		{
			FindPawn();
			if(ControlledPawn != nullptr)
			{
				ControlledPawn->WarningUpdate.Broadcast(Show, Progress);
			}
		}
	}
}

void ASFPlayerState::FindPawn()
{
	ControlledPawn = Cast<AShipPawn>(GetPawn());
}
