// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "SFPlayerState.generated.h"

class AShipPawn;

/**
 * 
 */
UCLASS()
class SPACEFLIGHT_API ASFPlayerState : public APlayerState
{
	GENERATED_BODY()

public:

	AShipPawn* ControlledPawn = nullptr;
	
	UFUNCTION(NetMulticast, Reliable)
	void FireWeaponMC();
	void FireWeaponMC_Implementation();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerFireWeapon();
	void ServerFireWeapon_Implementation();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerNotifyHit(AShipPawn* TargetHit);
	void ServerNotifyHit_Implementation(AShipPawn* TargetHit);

	UFUNCTION(Client, Reliable)
	void RPCShowHackWarning(bool Show, float Progress);
	void RPCShowHackWarning_Implementation(bool Show, float Progress);

private:

	void FindPawn();
};
