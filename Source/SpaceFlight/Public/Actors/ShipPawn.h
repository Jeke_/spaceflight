// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ShipPawn.generated.h"

UENUM(BlueprintType)
enum class EThrottleMode : uint8
{
	Idle,
	Slow,
	High,
	Full
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FVisualEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FEngineEvent, EThrottleMode, InMode);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FHackWarningEvent, bool, Show, float, Progress);

UCLASS()
class AShipPawn : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AShipPawn();

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, category = "Properties")
	int32 Health = 100.f;

	UPROPERTY(EditAnywhere, category = "Flight Characteristics")
	float PitchRate = 40.f;

	UPROPERTY(EditAnywhere, category = "Flight Characteristics")
	float RollRate = 100.f;

	UPROPERTY(EditAnywhere, category = "Flight Characteristics")
	float YawRate = 60.f;

	UPROPERTY(EditAnywhere, category = "Flight Characteristics")
	float MaxSpeed = 250.f;
	
	UPROPERTY(EditDefaultsOnly, category = "Input")
	float PitchDeadzone = 0.01f;

	UPROPERTY(EditDefaultsOnly, category = "Input")
	float YawDeadzone = 0.01f;

	UPROPERTY(EditDefaultsOnly, category = "Input")
	float RollDeadzone = 0.01f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EThrottleMode EngineSetting = EThrottleMode::Idle;

	UPROPERTY(BlueprintAssignable)
	FVisualEvent FireBeam;

	UPROPERTY(BlueprintAssignable)
	FEngineEvent EngineChange;

	UPROPERTY(BlueprintAssignable)
	FHackWarningEvent WarningUpdate;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void GetLifetimeReplicatedProps(TArray < FLifetimeProperty > & OutLifetimeProps) const override;

private:


	
	float CurrentPitch = 0.f;
	float CurrentRoll = 0.f;
	float CurrentYaw = 0.f;

	void Pitch(float AxisValue);
	void Roll(float AxisValue);
	void Yaw(float AxisValue);

	void EngineIdle();
	void EngineSlow();
	void EngineHigh();
	void EngineFull();
	
	void CompileControlValues();
	void HandleForwardMovement();

	FQuat EulerToQuaternion(FRotator Current_Rotation);

	void SetInputComponent();
};
