// Fill out your copyright notice in the Description page of Project Settings.


#include "DMGameMode.h"


#include "SFPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/GameNetworkManager.h"
#include "GameFramework/GameSession.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"

ADMGameMode::ADMGameMode()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ADMGameMode::BeginPlay()
{
	Super::BeginPlay();

}

void ADMGameMode::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

}

void ADMGameMode::RestartPlayer(AController* NewPlayer)
{
	if (NewPlayer == nullptr || NewPlayer->IsPendingKillPending())
	{
		return;
	}

	int32& PlayerID = *PlayerControllers.Find(NewPlayer);
	FString Tag = FString::FromInt(PlayerID);

	AActor* StartSpot = FindPlayerStart(NewPlayer, Tag);

	// If a start spot wasn't found,
	if (StartSpot == nullptr)
	{
		// Check for a previously assigned spot
		if (NewPlayer->StartSpot != nullptr)
		{
			StartSpot = NewPlayer->StartSpot.Get();
			UE_LOG(LogGameMode, Warning, TEXT("RestartPlayer: Player start not found, using last start spot"));
		}
	}

	RestartPlayerAtPlayerStart(NewPlayer, StartSpot);
}

void ADMGameMode::RestartPlayerAtPlayerStart(AController* NewPlayer, AActor* StartSpot)
{
	if (NewPlayer == nullptr || NewPlayer->IsPendingKillPending())
	{
		return;
	}

	if (!StartSpot)
	{
		UE_LOG(LogGameMode, Warning, TEXT("RestartPlayerAtPlayerStart: Player start not found"));
		return;
	}

	FRotator SpawnRotation = StartSpot->GetActorRotation();

	if (MustSpectate(Cast<APlayerController>(NewPlayer)))
	{
		UE_LOG(LogGameMode, Verbose, TEXT("RestartPlayerAtPlayerStart: Tried to restart a spectator-only player!"));
		return;
	}

	APawn* OldPawn = NewPlayer->GetPawn();

	if (GetDefaultPawnClassForController(NewPlayer) != nullptr)
	{
		// Try to create a pawn to use of the default class for this player
		NewPlayer->SetPawn(SpawnDefaultPawnFor(NewPlayer, StartSpot));
		ACharacter* NewChar = Cast<ACharacter>(NewPlayer->GetPawn());
		NewChar->GetCharacterMovement()->OnTimeDiscrepancyStrikeDetected.AddDynamic(this, &ADMGameMode::TimeDiscrepancyStrike);
	}

	if (NewPlayer->GetPawn() == nullptr)
	{
		NewPlayer->FailedToSpawnPawn();
	}
	else
	{
		if (OldPawn)
		{
			OldPawn->Destroy();
		}
		// Tell the start spot it was used
		InitStartSpot(StartSpot, NewPlayer);

		FinishRestartPlayer(NewPlayer, SpawnRotation);
	}
}

void ADMGameMode::PostLogin(APlayerController* NewPlayer)
{
	PlayerControllers.Add(NewPlayer, PlayerCount);
	ControllerStrikes.Add(NewPlayer, 0);
	PlayerCount++;
	
	Super::PostLogin(NewPlayer);
}

void ADMGameMode::TimeDiscrepancyStrike(AController* Controller)
{
	for (auto Element : ControllerStrikes)
	{
		if (ControllerStrikes.Find(Controller))
		{
			int32& Strikes = *ControllerStrikes.Find(Controller);
			Strikes++;
			UE_LOG(LogTemp, Error, TEXT("Strikes: %i"), *ControllerStrikes.Find(Controller));

			ASFPlayerState* PlayerState = Controller->GetPlayerState<ASFPlayerState>();
			if(PlayerState != nullptr)
			{
				PlayerState->RPCShowHackWarning(true, static_cast<float>(Strikes) / 20.f);
			}
			
			if(Strikes > 20)
			{
				FString String = "Speedhacking cheater";
				GameSession->KickPlayer(Cast<APlayerController>(Controller), FText::FromString(String));
			}
		}
	}
}