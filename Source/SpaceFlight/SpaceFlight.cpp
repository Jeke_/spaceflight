// Copyright Epic Games, Inc. All Rights Reserved.

#include "SpaceFlight.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SpaceFlight, "SpaceFlight" );
